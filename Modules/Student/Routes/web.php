<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('student')->group(function() {

    Route::get('/', 'StudentController@index')->name('index');

    Route::post('/store', 'StudentController@store')->name('store');

    Route::post('/add_department', 'DepartmentController@add_department')->name('add_department');

    Route::post('/save_subject', 'SubjectController@save_subject')->name('save_subject');

    Route::post('/delete/{id}', 'StudentController@delete')->name('delete');
});
