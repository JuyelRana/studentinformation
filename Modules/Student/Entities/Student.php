<?php

namespace Modules\Student\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\Marks\Entities\Marks;


class Student extends Model
{
    protected $guarded = [];

    public function marks()
    {
      return $this->hasOne(Marks::class);
    }
}
