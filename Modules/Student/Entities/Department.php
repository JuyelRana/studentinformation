<?php

namespace Modules\Student\Entities;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $guarded = [];
}
