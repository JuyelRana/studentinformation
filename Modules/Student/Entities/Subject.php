<?php

namespace Modules\Student\Entities;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $guarded = [
      'id','created_at','updated_at'
    ];
}
