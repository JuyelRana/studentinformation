<?php

namespace Modules\Student\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\Student\Entities\Student;
use Modules\Student\Entities\Department;

class StudentController extends Controller
{
  /**
  * Display a listing of the resource.
  * @return Response
  */
  public function index()
  {
    $students = Student::all();
    $departments = Department::all();

    $department_array = array();

    foreach ($departments as $department)
    {
      $department_array += array($department->id => $department->department_name);
    }

    return view('student::index', compact('students','department_array'));
  }

  /**
  * Show the form for creating a new resource.
  * @return Response
  */
  public function create()
  {
    return view('student::create');
  }

  /**
  * Store a newly created resource in storage.
  * @param Request $request
  * @return Response
  */
  public function store(Request $request)
  {
    Student::create($request->all());
    return 'Student Data Successfully Saved';
  }

  /**
  * Show the specified resource.
  * @param int $id
  * @return Response
  */
  public function show($id)
  {
    return view('student::show');
  }

  /**
  * Show the form for editing the specified resource.
  * @param int $id
  * @return Response
  */
  public function edit($id)
  {
    return view('student::edit');
  }

  public function delete($id)
  {
    $student = Student::find($id);
    $student->delete();
    return redirect()->route('index');;
  }

  /**
  * Update the specified resource in storage.
  * @param Request $request
  * @param int $id
  * @return Response
  */
  public function update(Request $request, $id)
  {
    //
  }

  /**
  * Remove the specified resource from storage.
  * @param int $id
  * @return Response
  */
  public function destroy($id)
  {
    //
  }
}
