<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Manage Student</title>
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
</head>
<body>

  @include('student::partials.navbar')

  @yield('content')
</body>


<!-- Add New Student Modal -->
<div class="modal fade" id="modal-create-student" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Add new Student</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <!-- Add new Student Form -->
      {{
        Aire::open()
        ->id('add_student_form')
        ->rules([
        'name' => 'alpha|required|max:20',
        'phone' => 'required|regex:/(01)[0-9]{9}/',
        'email' => 'required|email',
        ])
        ->messages([
        'name' => 'You must accept the terms',
        ])
      }}

      <!-- Student Name Field -->

      {{
        Aire::input('name')
        ->id('name')
        ->label('Name')
        ->type('text')
        ->placeholder('Name')
        ->autoComplete('off')
        ->class('form-control')
        ->groupClass('form-group')
      }}

      <!-- Student Phone Number Field -->

      {{
        Aire::input('phone')
        ->id('phone')
        ->label('Phone')
        ->type('text')
        ->placeholder('Phone')
        ->autoComplete('off')
        ->class('form-control')
        ->groupClass('form-group')
      }}

      <!-- Student Email Field -->

      {{
        Aire::input('email')
        ->id('email')
        ->label('Email')
        ->type('text')
        ->placeholder('Email')
        ->autoComplete('off')
        ->class('form-control')
        ->groupClass('form-group')
      }}

      <!-- Student Department Field -->
      {{
        Aire::select($department_array, 'department')
        ->label('Department')
        ->class('form-control')
        ->groupClass('form-group')
      }}

      <!-- Student Address Field -->
      {{
        Aire::input('address')
        ->id('address')
        ->label('Address')
        ->type('text')
        ->placeholder('Address')
        ->autoComplete('off')
        ->class('form-control')
        ->groupClass('form-group')
      }}


      {{
        Aire::close()
      }}
      <!-- End Add new Student Form -->

    </div>

    <div class="modal-footer">
      <button type="button" class="btn btn-light btn-sm" data-dismiss="modal">Close</button>
      <button type="button" class="btn btn-primary btn-sm" id="btn_save">Save</button>
    </div>
  </div>
</div>
</div>
<!-- End Add new student modal -->

<!-- Add Department -->
<div class="modal fade" id="modal-create-department" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Department</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        {{
          Aire::open()
          ->id('form-create-department')
          ->rules([
          'department_name' => 'alpha|required|max:20'
          ])
          ->messages([
          'department_name' => 'You must accept the terms',
          ])
        }}


        {{
          Aire::input('department_name')
          ->id('department_name')
          ->label('Department')
          ->type('text')
          ->placeholder('Department')
          ->autoComplete('off')
          ->class('form-control')
          ->groupClass('form-group')
        }}

        {{
          Aire::close()
        }}

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light btn-sm" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-sm" id="btn-save-department">Save Department</button>
      </div>
    </div>
  </div>
</div>
<!-- End Add Department -->

<!-- Add New Subject Modal -->
<div class="modal fade" id="modal-create-subject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add new Subject</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        {{
          Aire::open()
          ->id('form-create-subject')
          ->rules([
          'subject_name' => 'alpha|required'
          ])
          ->messages([
          'subject_name' => 'You must accept the terms',
          ])
        }}

        <!-- Student Department Field -->
        {{
          Aire::select($department_array, 'department_id')
          ->label('Department')
          ->class('form-control')
          ->groupClass('form-group')
        }}

        {{
          Aire::input('subject_name')
          ->id('subject_name')
          ->label('Subject')
          ->type('text')
          ->placeholder('Subject')
          ->autoComplete('off')
          ->class('form-control')
          ->groupClass('form-group')
        }}

        {{
          Aire::close()
        }}

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light btn-sm" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-sm" id="btn-save-subject">Save Subject</button>
      </div>
    </div>
  </div>
</div>
<!-- End Add new Subject modal -->


<!-- Edit Student -->
<div class="modal fade" id="btn_edit_student" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit a Student</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form id="form-edit-student">
          <input type="hidden" class="form-control" id="contact_id">
          <div class="form-group">
            <input type="text" class="form-control" name="first_name" placeholder="First Name">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="last_name" placeholder="Last Name">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="phone" placeholder="Phone">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="email" placeholder="Email">
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light btn-sm" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-sm" id="btn-update-student">Update Student</button>
      </div>
    </div>
  </div>
</div>

<!-- End Edit Student -->


<!-- JavaScript -->
<script src="{{ asset('js/jquery-3.3.1.slim.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>

<script>

$(document).ready(function () {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $(document).on('click', '#btn_save', function () {
    var createForm = $("#add_student_form");
    ajaxRequest(
      "{{ route('store') }}",
      'POST',
      createForm.serializeArray(),
      function (response) {
        // console.log(response.status);
        alert(response);
        $("#add_student_form").trigger("reset");
      });
    });

    // Add a new Department
    $(document).on('click', '#btn-save-department', function(){
      $('.text-danger').remove();
      var createForm = $("#form-create-department");
      ajaxRequest(
        "{{ route('add_department') }}",
        'POST',
        createForm.serializeArray(),
        function(response){
          // console.log(response);
          alert(response);
          $("#form-create-department").trigger("reset");
        });
      });
      // End Add a new Department


      // Add new Subject
      $(document).on('click', '#btn-save-subject', function(){
        $('.text-danger').remove();
        var createForm = $("#form-create-subject");
        ajaxRequest(
          "{{ route('save_subject') }}",
          'POST',
          createForm.serializeArray(),
          function(response){
            console.log(response);
            alert(response);
            $("#form-create-subject").trigger("reset");
          });
        });
        //End Add new Subject

      });


      function ajaxRequest(url, type, data, successFunction) {
        $.ajax({
          url: url,
          method: type,
          data: data,
          success: successFunction
        });

      }
      
      </script>
      <!-- End Ajax Script -->

      </html>
