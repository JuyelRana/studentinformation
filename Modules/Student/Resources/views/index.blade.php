@extends('student::layouts.master')

@section('content')
<table class="table table-striped">
  <tr>
    <th>SL</th>
    <th>Name</th>
    <th>Phone</th>
    <th>Email</th>
    <th>Address</th>
    <th>Mark</th>
    <th>Position</th>
    <th>Action</th>
  </tr>

  @php $i = 0; @endphp

  @foreach($students as $student)
  @php $i++; @endphp
  <tr>
    <td>{{$i}}</td>
    <td>{{ $student->name }}</td>
    <td>{{ $student->phone }}</td>
    <td>{{ $student->email }}</td>
    <td>{{ $student->address }}</td>
    <td>{{ $student->marks->marks }}</td>

    @if($student->marks->marks >= 80)
    <td>{{ "First" }}</td>
    @elseif($student->marks->marks >= 70 && $student->marks->marks <=79)
    <td>{{ "Second" }}</td>
    @elseif($student->marks->marks >= 60 && $student->marks->marks <=69)
    <td>{{ "Third" }}</td>
    @elseif($student->marks->marks >= 50 && $student->marks->marks <=59)
    <td>{{ "Fourth" }}</td>
    @elseif($student->marks->marks >= 40 && $student->marks->marks <=49)
    <td>{{ "Fifth" }}</td>
    @else
    <td>{{ "Fail" }}</td>
    @endif

    <td>
      <a href="#" class="btn btn-success" data-toggle="modal"
      data-target="#btn_edit_student">Edit</a>|
      <form action="{{ route('delete',$student->id) }}" method="post">
        {{ csrf_field() }}
        <input type="submit" class="btn btn-danger" value="Delete">
      </form>
    </td>
  </tr>
  @endforeach
</table>
@stop
