<nav class="navbar navbar-dark bg-dark navbar-expand-lg">
  <div class="container">

    <a class="navbar-brand" href="#">Dashboard</a>

    <ul class="navbar-nav navbar-right ml-auto">

      @role('admin')
      <li class="nav-item">
        <a class="nav-link" href="#" id="create-department" data-toggle="modal"
        data-target="#modal-create-department"><i class="fas fa-plus"></i>Add Department</a>
      </li>
      @endrole

      <li class="nav-item">
        <a class="nav-link" href="#" id="create-student" data-toggle="modal"
        data-target="#modal-create-student"><i class="fas fa-plus"></i>Add Student</a>
      </li>

      @role('admin')
      <li class="nav-item">
        <a class="nav-link" href="#" id="create-subject" data-toggle="modal"
        data-target="#modal-create-subject"><i class="fas fa-plus"></i>Add Subject</a>
      </li>
      @endrole

      <li class="nav-item">
        <a class="nav-link" href="#" id="create-marks" data-toggle="modal" data-target="#modal-create-marks"><i
          class="fas fa-plus"></i>Add Marks</a>
        </li>

      </ul>
    </div>
  </nav>
