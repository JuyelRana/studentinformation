<?php

namespace Modules\Marks\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\Marks\Entities\Marks;

use Modules\Student\Entities\Student;
use Modules\Student\Entities\Department;
use Modules\Student\Entities\Subject;


class MarksController extends Controller
{
  /**
  * Display a listing of the resource.
  * @return Response
  */
  public function index()
  {

    $students = Student::all();
    $departments = Department::all();
    $subjects = Subject::all();

    $department_array = array();
    $student_array = array();
    $subject_array = array();

    foreach ($departments as $department)
    {
      $department_array += array($department->id => $department->department_name);
    }

    foreach ($students as $student)
    {
      $student_array += array($student->id => $student->name);
    }

    foreach ($subjects as $subject)
    {
      $subject_array += array($subject->id => $subject->subject_name);
    }

    return view('marks::index', compact('students','departments','subjects','department_array','student_array','subject_array'));
  }

  /**
  * Show the form for creating a new resource.
  * @return Response
  */
  public function create()
  {
    return view('marks::create');
  }

  /**
  * Store a newly created resource in storage.
  * @param Request $request
  * @return Response
  */
  public function store(Request $request)
  {
    Marks::create($request->all());
    return 'Marks Successfully Saved';
  }

  // public function save_marks(Request $request)
  // {
  //   return $request;
  // }


  /**
  * Show the specified resource.
  * @param int $id
  * @return Response
  */
  public function show($id)
  {
    return view('marks::show');
  }

  /**
  * Show the form for editing the specified resource.
  * @param int $id
  * @return Response
  */
  public function edit($id)
  {
    return view('marks::edit');
  }

  /**
  * Update the specified resource in storage.
  * @param Request $request
  * @param int $id
  * @return Response
  */
  public function update(Request $request, $id)
  {
    //
  }

  /**
  * Remove the specified resource from storage.
  * @param int $id
  * @return Response
  */
  public function destroy($id)
  {
    //
  }
}
