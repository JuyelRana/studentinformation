<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('marks')->group(function() {
    Route::get('/', 'MarksController@index');

    Route::post('/store', 'MarksController@store')->name('save_marks');
});
