<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Manage Student</title>
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
</head>
<body>

  @include('marks::partials.navbar')

  @yield('content')

</body>

<!-- Add Marks Modal -->
<div class="modal fade" id="modal-create-marks" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Mark</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <!-- Add new Marks Form -->
        {{
          Aire::open()
          ->id('form-create-marks')
          ->rules([
          'marks' => 'integer|required',
          ])
          ->messages([
          'marks' => 'You must accept the terms',
          ])
        }}

        <!-- Student Department Field -->
        {{
          Aire::select($department_array, 'department_id')
          ->label('Department')
          ->class('form-control')
          ->groupClass('form-group')
        }}

        <!-- Student Department Field -->
        {{
          Aire::select($student_array, 'student_id')
          ->label('Student')
          ->class('form-control')
          ->groupClass('form-group')
        }}
        <!-- Student Department Field -->
        {{
          Aire::select($subject_array, 'subject_id')
          ->label('Subject')
          ->class('form-control')
          ->groupClass('form-group')
        }}

        {{
          Aire::input('marks')
          ->id('marks')
          ->label('Marks')
          ->type('text')
          ->placeholder('Marks')
          ->autoComplete('off')
          ->class('form-control')
          ->groupClass('form-group')
        }}


        {{
          Aire::close()
        }}

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light btn-sm" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-sm" id="btn-save-marks">Save Marks</button>
      </div>
    </div>
  </div>
</div>
<!-- End Add Marks modal -->

<!-- JavaScript -->
<script src="{{ asset('js/jquery-3.3.1.slim.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>

<script>

$(document).ready(function(){

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  // Add Marks
  $(document).on('click', '#btn-save-marks', function(){
    $('.text-danger').remove();
    var createForm = $("#form-create-marks");
    ajaxRequest(
      "{{ route('save_marks') }}",
      'POST',
      createForm.serializeArray(),
      function(response){
        console.log(response);
        alert(response);
        $("#form-create-marks").trigger("reset");
      });
    });
    //End Add Marks


  });

  function ajaxRequest(url, type, data, successFunction){
    $.ajax({
      url: url,
      method: type,
      data: data,
      success: successFunction
    });
  }

</script>



</html>
