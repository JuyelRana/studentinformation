<nav class="navbar navbar-dark bg-dark navbar-expand-lg">
  <div class="container">

    <a class="navbar-brand" href="#">Dashboard</a>

    <ul class="navbar-nav navbar-right ml-auto">

      <li class="nav-item">
        <a class="nav-link" href="#" id="create-marks" data-toggle="modal" data-target="#modal-create-marks"><i
          class="fas fa-plus"></i>Add Marks</a>
      </li>
      </ul>
    </div>
  </nav>
