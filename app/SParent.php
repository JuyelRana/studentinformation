<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Modules\Student\Entities\Student;

class SParent extends Model
{
  protected $guarded = [];

  public function students()
  {
    return $this->belongsTo(Student::class,'student_id');

  }

}
