<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Modules\Student\Entities\Student;

use App\SParent;

class HomeController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Contracts\Support\Renderable
  */
  public function index()
  {
    $students = Student::all();

    $sparents = SParent::all();

    $student_array = array();

    foreach ($students as $student)
    {
      $student_array += array($student->id => $student->name);
    }

    return view('home',compact('student_array','sparents'));

  }
}
