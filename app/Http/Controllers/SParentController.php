<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\SParent;

class SParentController extends Controller
{
    public function add_parent(Request $request)
    {
        SParent::create($request->all());

        return 'Parent Data Successfully Saved';
    }
}
