<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
  <div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
      <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
          {{ config('app.name', 'Laravel') }}
        </a>


        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Left Side Of Navbar -->
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link" href="#" id="create-parent" data-toggle="modal"
              data-target="#modal-create-parent"><i class="fas fa-plus"></i>Add Parent</a>
            </li>
          </ul>

          <!-- Right Side Of Navbar -->
          <ul class="nav navbar-nav navbar-right">
            <!-- Authentication Links -->
            @if (Auth::guest())
            <li><a href="{{ route('login') }}">Login</a></li>
            <li><a href="{{ route('register') }}">Register</a></li>
            @else
            <!-- ADD ROLES AND USERS LINKS -->
            @role('admin')
            <li><a href="{{ route('roles.index') }}">Roles</a></li>
            <li><a href="{{ route('users.index') }}">Users</a></li>
            @endrole

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
              </a>

              <ul class="dropdown-menu" role="menu">
                <li>
                  <a href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                  Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form>
              </li>
            </ul>
          </li>
          @endif
        </ul>>

      </div>
    </div>
  </nav>

  <main class="py-4">
    @yield('content')
  </main>


</div>
</body>


<!-- Add New Parent Modal -->
<div class="modal fade" id="modal-create-parent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Add Parent</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <!-- Add new Student Form -->
      {{
        Aire::open()
        ->id('add_parent_form')
        ->route('add_parent')
        ->rules([
        'name' => 'alpha|required|max:20',
        'phone' => 'required|regex:/(01)[0-9]{9}/',
        'email' => 'required|email',
        ])
        ->messages([
        'name' => 'You must accept the terms',
        ])
      }}

      <!-- Parent Name Field -->

      {{
        Aire::input('name')
        ->id('name')
        ->label('Name')
        ->type('text')
        ->placeholder('Name')
        ->autoComplete('off')
        ->class('form-control')
        ->groupClass('form-group')
      }}

      <!-- Parent Phone Number Field -->

      {{
        Aire::input('phone')
        ->id('phone')
        ->label('Phone')
        ->type('text')
        ->placeholder('Phone')
        ->autoComplete('off')
        ->class('form-control')
        ->groupClass('form-group')
      }}

      <!-- Parent Email Field -->

      {{
        Aire::input('email')
        ->id('email')
        ->label('Email')
        ->type('text')
        ->placeholder('Email')
        ->autoComplete('off')
        ->class('form-control')
        ->groupClass('form-group')
      }}

      <!-- Parent Address Field -->
      {{
        Aire::input('address')
        ->id('address')
        ->label('Address')
        ->type('text')
        ->placeholder('Address')
        ->autoComplete('off')
        ->class('form-control')
        ->groupClass('form-group')
      }}

      {{
        Aire::select($student_array, 'student_id')
        ->label('Student')
        ->class('form-control')
        ->groupClass('form-group')
      }}

      {{
        Aire::close()
      }}
      <!-- End Add new Parent Form -->

    </div>

    <div class="modal-footer">
      <button type="button" class="btn btn-light btn-sm" data-dismiss="modal">Close</button>
      <button type="button" class="btn btn-primary btn-sm" id="btn_save">Save</button>
    </div>
  </div>
</div>
</div>
<!-- End Add new parent modal -->



<!-- JavaScript -->
<script src="{{ asset('js/jquery-3.3.1.slim.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>

<script>

$(document).ready(function(){

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  // Add Marks
  $(document).on('click', '#btn_save', function(){
    $('.text-danger').remove();
    var createForm = $("#add_parent_form");
    ajaxRequest(
      "{{ route('add_parent') }}",
      'POST',
      createForm.serializeArray(),
      function(response){
        console.log(response);
        alert(response);
        $("#add_parent_form").trigger("reset");
      });
    });
    //End Add Marks


  });

  function ajaxRequest(url, type, data, successFunction){
    $.ajax({
      url: url,
      method: type,
      data: data,
      success: successFunction
    });
  }

</script>

</html>
