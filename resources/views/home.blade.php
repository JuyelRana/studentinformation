@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">Dashboard</div>

        <table class="table table-striped">
          <tr>
            <th>SL</th>
            <th>Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Address</th>
            <th>Student</th>
            <th>Marks</th>
            <th>Action</th>
          </tr>

          @php $i = 0; @endphp

          @foreach($sparents as $sparent)
          @php $i++; @endphp
          <tr>
            <td>{{$i}}</td>
            <td>{{ $sparent->name }}</td>
            <td>{{ $sparent->phone }}</td>
            <td>{{ $sparent->email }}</td>
            <td>{{ $sparent->address }}</td>
            <td>{{ $sparent->students->name }}</td>
            <td>{{ $sparent->students->marks->marks }}</td>

            <td>
              <a href="#" class="btn btn-success" data-toggle="modal"
              data-target="#btn_edit_student">Edit</a>|
              <form action="" method="post">
                {{ csrf_field() }}
                <input type="submit" class="btn btn-danger" value="Delete">
              </form>
            </td>
          </tr>
          @endforeach
        </table>

        <div class="card-body">
          @if (session('status'))
          <div class="alert alert-success" role="alert">
            {{ session('status') }}
          </div>
          @endif

          You are logged in!
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
